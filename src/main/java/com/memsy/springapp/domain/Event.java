package com.memsy.springapp.domain;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Calendar;
import java.util.Objects;

public class Event {

    private Integer id;

    @NotEmpty(message = "Summary is required")
    private String summary;

    @NotEmpty(message = "Description is required")
    private String description;

    @NotNull(message = "When is required")
    private Calendar when;

    @NotNull(message = "Calendar user is required")
    private CalendarUser owner;

    private CalendarUser attendee;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Calendar getWhen() {
        return when;
    }

    public void setWhen(Calendar when) {
        this.when = when;
    }

    public CalendarUser getOwner() {
        return owner;
    }

    public void setOwner(CalendarUser owner) {
        this.owner = owner;
    }

    public CalendarUser getAttendee() {
        return attendee;
    }

    public void setAttendee(CalendarUser attendee) {
        this.attendee = attendee;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, summary, description, when, owner, attendee);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final Event other = (Event) obj;
        return Objects.equals(this.id, other.id)
                && Objects.equals(this.summary, other.summary)
                && Objects.equals(this.description, other.description)
                && Objects.equals(this.when, other.when)
                && Objects.equals(this.owner, other.owner)
                && Objects.equals(this.attendee, other.attendee);
    }

    @Override
    public String toString() {
        return "Event{" +
                "id=" + id +
                ", summary='" + summary + '\'' +
                ", description='" + description + '\'' +
                ", when=" + when +
                ", owner=" + owner +
                ", attendee=" + attendee +
                '}';
    }
}
