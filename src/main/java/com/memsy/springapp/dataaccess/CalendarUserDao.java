package com.memsy.springapp.dataaccess;

import com.memsy.springapp.domain.CalendarUser;

import java.util.List;

public interface CalendarUserDao {

    CalendarUser getUser(int id);

    CalendarUser findUserByEmail(String email);

    List<CalendarUser> findUsersByEmail(String partialEmail);

    int createUser(CalendarUser user);
}
