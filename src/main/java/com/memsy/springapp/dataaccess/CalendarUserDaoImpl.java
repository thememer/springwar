/*
package com.memsy.springapp.dataaccess;

import com.memsy.springapp.domain.CalendarUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class CalendarUserDaoImpl implements CalendarUserDao {

    private final JdbcOperations jdcJdbcOperations;

    private static final String CALENDAR_USER_QUERY = "select id, email, password, first_name, last_name from calendar_users where ";

    private static final RowMapper<CalendarUser> CALENDAR_USER_MAPPER = new CalendarUserRowMapper("calendar_users.");


    @Autowired
    public CalendarUserDaoImpl(JdbcOperations jdbcOperations) {
        if (jdbcOperations == null) {
            throw new IllegalArgumentException("jdbcOperations cannot be null");
        }
        this.jdcJdbcOperations = jdbcOperations;
    }

    @Override
    @Transactional(readOnly = true)
    public CalendarUser getUser(int id) {
        return jdcJdbcOperations.queryForObject(CALENDAR_USER_QUERY + "id = ?", CALENDAR_USER_MAPPER, id);
    }

    @Override
    @Transactional(readOnly = true)
    public CalendarUser findUserByEmail(String email) {
        if(email == null) {
            throw new IllegalArgumentException("Email cannot be null");
        }
        try{
            return jdcJdbcOperations.queryForObject(CALENDAR_USER_QUERY + "email = ?", CALENDAR_USER_MAPPER, email);
        }catch(EmptyResultDataAccessException notFound){
            return null;
        }
    }

    @Override
    public List<CalendarUser> findUsersByEmail(String partialEmail) {
        if(partialEmail ==  null){
            throw new IllegalArgumentException("Email cannot be null");
        }
        if("".equals(partialEmail)){
            throw new IllegalArgumentException("Email cannot be empty string");
        }
        return jdcJdbcOperations.query(CALENDAR_USER_QUERY + "email like ? order by id", CALENDAR_USER_MAPPER, partialEmail + "%");
    }

    @Override
    public int createUser(final CalendarUser user) {
        if(user == null){
            throw new IllegalArgumentException("User cannot be null");
        }
        if(user.getId() != null){
            throw new IllegalArgumentException("User.getId() must be null when creating a " + CalendarUser.class.getName() );
        }

        KeyHolder keyHolder = new GeneratedKeyHolder();
        this.jdcJdbcOperations.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                PreparedStatement ps = con.prepareStatement("insert into calendar_users(email, password, first_name, last_name) values (?,?,?,?)",new String[]{"id"});
                ps.setString(1, user.getEmail());
                ps.setString(2, user.getPassword());
                ps.setString(3, user.getFirstName());
                ps.setString(4, user.getLastName());
                return ps;
            }
        }, keyHolder);

        return keyHolder.getKey().intValue();
    }

    static class CalendarUserRowMapper implements RowMapper<CalendarUser> {
        private final String columnLabelPrefix;

        public CalendarUserRowMapper(String columnLabelPrefix){
            this.columnLabelPrefix = columnLabelPrefix;
        }

        public CalendarUser mapRow(ResultSet rs, int rowNum) throws SQLException {
            CalendarUser user = new CalendarUser();
            user.setId(rs.getInt(columnLabelPrefix + "id"));
            user.setEmail(rs.getString(columnLabelPrefix + "email"));
            user.setPassword(rs.getString(columnLabelPrefix + "password"));
            user.setFirstName(rs.getString(columnLabelPrefix + "first_name"));
            user.setLastName(rs.getString(columnLabelPrefix + "last_name"));
            return user;
        }
    }
}
*/
